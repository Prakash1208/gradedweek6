package com.hcl.movie;

public class Movie {

	private Integer id;
	private String title;
	private String status;
	private String country;
	private Double rating;


	private Movie() {
	super();
	}
	public Movie(Integer id, String title, String status, String country, Double rating) {
	super();
	this.id=id;
	this.title = title;
	this.status = status;
	this.country = country;
	this.rating = rating;
	}



	public Integer getId() {
	return id;
	}


	public void setId(Integer id) {
	this.id = id;
	}


	public String getTitle() {
	return title;
	}
	public void setTitle(String title) {
	this.title = title;
	}
	public String getStatus() {
	return status;
	}
	public void setStatus(String status) {
	this.status = status;
	}
	public String getCountry() {
	return country;
	}
	public void setCountry(String country) {
	this.country = country;
	}
	public Double getRating() {
	return rating;
	}
	public void setRating(Double rating) {
	this.rating = rating;
	}
	@Override
	public String toString() {
	return "Movie [title=" + title + ", status=" + status + ", country=" + country + ", rating=" + rating + "]\n";
	}

}
