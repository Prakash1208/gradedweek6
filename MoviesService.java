package com.hcl.movie;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class MoviesService {
	
	private List<Movie> allMovies;
	private List<Movie> favMovies;


	public static MoviesService moviesService=new MoviesService();
	private MoviesService() {

	}
	//singleton
	public static MoviesService getServiceObject() {
	if(moviesService!=null)
	return moviesService;
	moviesService=new MoviesService();
	return moviesService;
	}

	public void setAllMovies(List<Movie> allMovies) {
	this.allMovies=allMovies;
	favMovies=new ArrayList<>();
	}

	//first question
	public List<Movie> getComingSoonMovies(){
	return allMovies.stream().filter(movie->movie.getStatus().equalsIgnoreCase("comingsoon")).collect(Collectors.toList());
	}

	public List<Movie> getMoviesInTheatres(){
	return allMovies.stream().filter(movie->movie.getStatus().equalsIgnoreCase("moviesintheatre")).collect(Collectors.toList());
	}
	public List<Movie> getTopRatedIndian(){
	List<Movie> indianMovies=allMovies.stream().filter(movie->movie.getCountry().equalsIgnoreCase("india")).collect(Collectors.toList());
	Collections.sort(indianMovies,(movie1,movie2)->movie1.getRating()>movie2.getRating()?-1:1);
	return indianMovies;
	}

	public List<Movie> getTopRatedMovies(){
	Collections.sort(allMovies,(movie1,movie2)->movie1.getRating()>movie2.getRating()?-1:1);
	return allMovies;
	}
	//end of first question

	public List<String> getAllMoviesTitle(){
	List<String> allMoviesInfo=new ArrayList<>();
	for(Movie movie:allMovies)
	allMoviesInfo.add(movie.getTitle());
	return allMoviesInfo;
	}
	public Movie getMovie(int id) {
	return allMovies.stream().
	    filter(movie -> movie.getId()==id).
	    findFirst().orElse(null);
	}

	public boolean addFavMovie(Movie movie) {
	return favMovies.add(movie);
	}

	public List<Movie> getFavMovies(){
	return favMovies;
	}

	public boolean removeFavMovie(Movie movie) {
	return favMovies.remove(movie);
	}

}
