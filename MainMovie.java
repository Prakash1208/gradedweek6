package com.hcl.movie;

import java.util.ArrayList;
import java.util.List;

public class MainMovie {

	public static void main(String[] args) {

		List<Movie> allMovies = new ArrayList<>();
		
		allMovies.add(new Movie(1, "Hobbit", "In theater", "USA", 7.6));
		
		allMovies.add(new Movie(2, "THE-GOD FATHER", "Top-Rated", "UK", 9.2));
		
		allMovies.add(new Movie(3, "RadheSyam", "Coming Soon", "INDIA", 0.0));
		
		allMovies.add(new Movie(4, "THE-BATMAN", "Coming Soon", "india", 0.0));
		
		allMovies.add(new Movie(5, "THE-DARK-KNIGHT", "Released", "us", 9.0));

		allMovies.add(new Movie(6, "KUNG-FU-HUSTLE", "Released", "China", 6.2));

		System.out.println(allMovies);
		
		// singleton
		
		MoviesService service = MoviesService.getServiceObject();
		service.setAllMovies(allMovies);

		System.out.println(service.getComingSoonMovies());

		System.out.println(service.getMoviesInTheatres());

		System.out.println(service.getTopRatedIndian());

		System.out.println(service.getTopRatedMovies());
		

		System.out.println(service.getMovie(1));
		
		Movie movie = service.getMovie(4);
		
		service.addFavMovie(movie);
		
		System.out.println(service.getFavMovies());
		
		service.removeFavMovie(movie);
		
		System.out.println(service.getFavMovies());
		
		
	}

}
